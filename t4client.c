#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#define PORT ("3490")
#define IPSTR_LEN (INET6_ADDRSTRLEN + 1)

int iswriting = 0;

void * get_in_addr(struct sockaddr * sa)
{
	if (AF_INET == sa->sa_family)
	{
		return &( (struct sockaddr_in *)sa)->sin_addr;
	}
	return &( (struct sockaddr_in6 *)sa)->sin6_addr;
}




char * get_ipstrs(const char * DNS_name, size_t * count)
{
	struct addrinfo hints 			= {0};
	hints.ai_family 				= AF_UNSPEC;
	hints.ai_socktype				= 0; // both TCP and UDP(SOCK_STREAM and SOCK_DGRAM respectively)
	hints.ai_protocol				= 0;
	hints.ai_flags					= AI_PASSIVE;
	
	struct addrinfo * res;
	
	int retval;
	if (0 != (retval = getaddrinfo(DNS_name, NULL, &hints, &res) ) )
	{
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(retval) );
		exit(2);
	}
	
	struct addrinfo * node;
	
	size_t ipcount = 0;
	
	for(node = res; NULL != node; node = node->ai_next)
	{
		++ipcount;
	}
	*count = ipcount;
	
	const size_t ipstrs_size = ipcount * IPSTR_LEN * sizeof(char);
	char * const ipstrs = (char * const)(malloc(ipstrs_size) );
	memset(ipstrs, '\0', ipcount * IPSTR_LEN * sizeof(char) );
	
	size_t i = 0;
	for(node = res; NULL != node; node = node->ai_next)
	{
		if (NULL == inet_ntop(node->ai_family, get_in_addr(node->ai_addr), &ipstrs[IPSTR_LEN * i++], IPSTR_LEN * sizeof(char) ) )
		{
			perror("client inet_ntop error");
			exit(3);
		}
	}
	
	freeaddrinfo(res);

	return ipstrs;
}

int main(int argc, char * argv[])
{
	if (2 != argc)
	{
		puts("usage: <DNS_Name>");
		exit(1);
	}
	size_t ipstrs_count;
	char * const ipstrs = get_ipstrs(argv[1], &ipstrs_count);
	
	struct addrinfo hints 	= {0};
	hints.ai_family			= AF_UNSPEC;
	hints.ai_socktype		= SOCK_STREAM;
	hints.ai_flags			= AI_PASSIVE;
	
	struct addrinfo * res;
	
	int retval;
	if (0 != (retval = getaddrinfo(NULL, PORT, &hints, &res) ) )
	{
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(retval) );
		exit(4);
	}
	
	struct addrinfo * node;
	int sockfd;
	
	
	for(node = res; NULL != node; node = node->ai_next)
	{
		sockfd = socket(node->ai_family, node->ai_socktype, node->ai_protocol);
		if (-1 == sockfd)
		{
			perror("client socket error");
			continue;
		}
		if (0 != connect(sockfd, node->ai_addr, node->ai_addrlen) )
		{
			close(sockfd);
			perror("client connect error");
			continue;
		}
		
		break;
	}
	
	if (NULL == node)
	{
		fprintf(stderr, "client failed to connect\n");
		exit(5);
	}
	
	char str[INET6_ADDRSTRLEN];
	if (NULL == inet_ntop(node->ai_family, get_in_addr( (struct sockaddr *)node->ai_addr), str, sizeof(str) ) )
	{
		perror("client inet_ntop error");
	}
	freeaddrinfo(res);
	
	
	if (-1 == send(sockfd, *(argv + 1), strlen( *(argv + 1) ) + 1, 0) )
	{
		perror("client send error");
		exit(6);
	}
	
	size_t i;
	for(i = 0; i < ipstrs_count; ++i)
	{
		puts(ipstrs + i * IPSTR_LEN);
		if (-1 == send(sockfd, ipstrs + i * IPSTR_LEN, strlen(ipstrs + i * IPSTR_LEN) + 1, 0) )
		{
			perror("client send error");
			exit(7);
		}
	}
	
	free(ipstrs);
	close(sockfd);
	
	return 0;
}
