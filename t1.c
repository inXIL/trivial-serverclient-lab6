#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(const int argc, char* argv[]) 
{
	if (2 != argc || (0 != strcmp("raw", argv[1]) && 0 != strcmp("formatted", argv[1]) ) ) 
	{
		puts("usage: \"task1 raw\" or \"task1 formatted\"\n");
		exit(1);
	}
	
	srand((unsigned)time(NULL) );
	size_t const cnt_megabyte = 2;
	size_t const size = cnt_megabyte * 1024 * 1024;
	char * const text = (char * const)malloc(size + 1);
	if (NULL == text) 
	{
		perror("failed to allocate memory buffer");
		exit(2);
	}
	
	for(size_t i = 0; i < size; ++i)
	{
		text[i] = (char)('a' + rand() % 26);
	}
	text[size - 1] = '\0';
	
	if (0 == strcmp("formatted", argv[1]) )
	{
		FILE* file = fopen("file1.txt",  "w");
		if (NULL == file) 
		{
			perror("can't open the file file1.txt");
			exit(3);
		}
		// see (1) for opinionated reasoning
		fprintf(file, "%s", text);
		
		fclose(file);
	}
	else
	{
		int fd = open("file1.txt", O_RDWR, 0666); // octal 666 is rw-rw-rw-
		if (-1 == fd) 
		{
			perror("can't open the file file1.txt");
			exit(4);
		}
		// wanted to use atexit here, but failed to think of a way to pass params in a reasonable way
		write(fd, text, size - 1); // last char '\0'-ed to accomodate for "%s"
		close(fd);
	}
	
	free(text);
	return 0;
}	
	// (1):
	// i don't know what should i specify in fprintf cause char loop is unfair
	// (kernel calls, function prologue and epilogue...)
	// while writing as string does not comply with the task at hand
	// we could try to profile kernel and user time separately, but it's still not a great option
	// ultimately printf should call write in the end. . .
	// and do preprocessing in the userspace
	// then i suppose time difference should be negligible if it's used properly
