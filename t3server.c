#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#define FIFO_NAME ("FIFO_NAME")
#define MAX_SIZE (30)
int main()
{
	if (0 != mkfifo(FIFO_NAME, 0666))  
	{
		perror("mkfifo error");
		exit(2);
	}
	int fd = open(FIFO_NAME, O_RDONLY);
	if (-1 == fd)
	{
		perror("open error");
		exit(1);
	}
	puts("unblocked");
	char str[MAX_SIZE + 1];
	ssize_t res = read(fd, &str, MAX_SIZE);
	if (0 >= res) {
		perror("read error");
	}
	str[res] = '\0';
	for(int i = 0; i < 10; ++i) {
		puts(str);
	}
	return 0;
}

