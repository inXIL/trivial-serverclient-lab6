// writes argv[1] to FIFO after SIGINT and quits

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include <signal.h>
#include <bits/sigaction.h>

#define FIFO_NAME ("FIFO_NAME")

const char * str;

void sigint_handler(int sig)
{
	// how to pass argv[1] and fd here?
	// signal is much more user-friendly, it seems
	// i don't alter globals so i can use them
	int fd = open(FIFO_NAME, O_WRONLY);
	if (-1 == fd) {
		perror("open error:");
		exit(3);
	}
	
	if (-1 == write(fd, str, strlen(str) ) ) {
		perror("write error");
		exit(5);
	}
	close(fd);
	
	puts("sending to fifo and exiting");
}


int main(const int argc, char * argv[])
{
	if (2 != argc)
	{
		puts("usage: task3_FIFO_client <string>");
		exit(1);
	}
	str = argv[1];
	
	struct sigaction sa = {0};
	sa.sa_handler 		= sigint_handler;
	sa.sa_flags 		= 0;
	
	if (0 != sigaction(SIGINT, &sa, NULL) ) { // SIGINT is the worst choice, but it works
		perror("sigaction error:");
		exit(4);
	}
	sleep(271828182);
	return 0;
}
