CC=gcc

#valgrind requires "-g -O0", gdb requires "-g"
#some of the flags to dev C without completely losing sanity
CFLAGS=-g -O0 -Werror -Wno-error=unused-parameter -Wall -Wextra -Wshadow -Wformat=2 -Wfloat-equal -Wconversion -Wlogical-op -Wshift-overflow=2 -Wduplicated-cond -Wcast-qual -Wcast-align #-fsanitize=address -fsanitize=undefined -fno-sanitize-recover -fstack-protector

#valgrind flags
VFLAGS=--leak-check=full -s

.PHONY: t3 t4 all clean

#tn = task n
all: t1 t2 t3 t4
	
clean: 
	rm -f *.o

t1: t1.o 
	$(CC) -std=c99 $(CFLAGS) t1.c -o t1.o
	valgrind $(VFLAGS) --log-file="valgrind-logs/t1_raw.txt" 	./t1.o raw
	valgrind $(VFLAGS) --log-file="valgrind-logs/t1_formatted.txt" 	./t1.o formatted

t2: t2.o
	$(CC) -std=c99 -lpthread $(CFLAGS) t2.c -o t2.o
	valgrind $(VFLAGS) --log-file="valgrind-logs/t2.txt" 		./t2.o

t3: t3server.o t3client.o
	$(CC) -std=c99 $(CFLAGS) t3server.c -o t3server.o
	$(CC) $(CFLAGS) t3client.c -o t3client.o


t4: t4server.o t4client.o
	$(CC) -lpthread $(CFLAGS) t4server.c -o t4server.o
	$(CC) $(CFLAGS) t4client.c -o t4client.o
	valgrind $(VFLAGS) --log-file="valgrind-logs/t4server.txt"	./t4server.o &
	valgrind $(VFLAGS) --log-file="valgrind-logs/t4client.txt"	./t4client.o ya.ru 
