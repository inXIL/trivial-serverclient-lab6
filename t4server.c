#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <stdatomic.h>

#define PORT ("3490")
#define BACKLOG (10)
#define MAXDATASIZE (100)

atomic_int is_writing;

void * get_in_addr(struct sockaddr * sa)
{
	if (AF_INET == sa->sa_family)
	{
		return &( (struct sockaddr_in *)sa)->sin_addr;
	}
	return &( (struct sockaddr_in6 *)sa)->sin6_addr;
}

int recv_ipstr_to_buf(int fd, char * buf)
{
	size_t i = 0;
	while(1)
	{
		ssize_t byte_count = recv(fd, buf + i, 1, 0);
		if (-1 == byte_count)
		{
			perror("server recv error");
			exit(8);
		}
		if (0 == byte_count)
		{
			return 0;
		}
		if ('\0' == buf[i])
		{
			return 1;
		}
		++i;
	}
}

void * client_handler(void * pfd)
{
	int a = 0;
	while(!atomic_compare_exchange_weak(&is_writing, &a, 1) ) 
	{ 
		/* do nothing */
	}
	int fd = open("ip_list.txt", O_APPEND | O_CREAT | O_WRONLY, 0666);
	if (-1 == fd)
	{
		perror("server open error");
		exit(42);
	}
	char buf[MAXDATASIZE];
	char newline = '\n';
	while(recv_ipstr_to_buf(*(int *)pfd, buf) )
	{
		if (-1 == write(fd, buf, strlen(buf) ) )
		{
			perror("server write error");
			exit(43);
		}
		if (-1 == write(fd, &newline, 1) )
		{
			perror("server write error");
			exit(44);
		}
	}
	if (-1 == write(fd, &newline, 1) )
	{
		perror("server write error");
		exit(45);
	}
	close(fd);
	atomic_store(&is_writing, 0);
	return NULL;
}

int main()
{
	struct addrinfo hints 	= {0};
	hints.ai_family 		= AF_UNSPEC;
	hints.ai_socktype		= SOCK_STREAM;
	hints.ai_flags			= AI_PASSIVE;
	
	struct addrinfo * server_info;
	
	int retval;
	if (0 != (retval = getaddrinfo(NULL, PORT, &hints, &server_info) ) )
	{
		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(retval) );
		exit(1);
	}
	
	struct addrinfo * node = server_info;
	int sockfd = -1;
	int yes = 1;
	for(node = server_info; NULL != node; node = node->ai_next)
	{
		sockfd = socket(node->ai_family, node->ai_socktype, node->ai_protocol);
		if (-1 == sockfd)
		{
			continue;
		}
		if (0 != setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int) ) )
		{
			perror("server setsockopt error");
			exit(2);
		}
		if (-1 == bind(sockfd, node->ai_addr, node->ai_addrlen) )
		{
			close(sockfd);
			perror("server bind error");
			continue;
		}
		
		break;
	}
	
	freeaddrinfo(server_info);
	
	if (NULL == node)
	{
		fprintf(stderr, "server failed to bind\n");
		exit(3);
	}
	
	if (0 != listen(sockfd, BACKLOG) )
	{
		perror("server listen error");
		exit(4);
	}
	
	printf("server: waiting for connections. . .\n");
	
	struct sockaddr_storage client_addr;
	
	while(1)
	{
		socklen_t sin_size = sizeof(client_addr);
		int fd_i = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);
		if (-1 == fd_i)
		{
			perror("server accept error");
			continue;
		}
		
		char str[INET6_ADDRSTRLEN];
		if (NULL == inet_ntop(client_addr.ss_family, get_in_addr((struct sockaddr *)&client_addr), str, sizeof(str) ) )
		{
			perror("server inet_ntop error");
		}
		else
		{
			printf("server got connection from %s\n", str);
		}
		
		pthread_t t;
		if (0 != pthread_create(&t, NULL, client_handler, &fd_i) )
		{
			perror("server pthread_create error");
			exit(6);
		}
		if (0 != pthread_detach(t) )
		{
			perror("server pthread_detach error");
			exit(7);
		}
	}
	
	return 0;
}
