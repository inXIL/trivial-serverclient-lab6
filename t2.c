//#define __STDC_WANT_LIB_EXT1__ 1
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>


char * file_to_buffer(FILE * const file, size_t *size) {
	if (0 != fseek(file, 0, SEEK_END) ) {
		perror("fseek error");
		return NULL;
	}
	long const length = ftell(file);
	if (-1 == length) {
		perror("ftell error");
		return NULL;
	}
	if (0 != fseek(file, 0, SEEK_SET) ) {
		perror("fseek error");
		return NULL;
	}
	char * buffer = (char *)malloc((size_t)length + 1);
	if (NULL != size) {
		*size = (size_t)length + 1;
	}
	buffer[length] = '\0';
	if (NULL == buffer) {
		perror("failed to alloc buffer");
		return NULL;
	}
	if (length != (long long)fread(buffer, 1, (size_t)length, file) ) {
		perror("fread error");
		return NULL;
	}
	return buffer;
}

char * clean_word(char * word)
// well... this one is bad.
// writing natural language lexer would be too much
{	
#define MAX_LEN (100)
	static char s[MAX_LEN];
	size_t i, k;
	for(i = 0, k = 0; '\0' != word[i] && k < MAX_LEN; ++i) {
	        if (isalpha(word[i]) || isdigit(word[i]) || '-' == word[i] || '\'' == word[i]) {
		// let's keep hyphen
		// actually i don't know how to deal
		// with those characters in pure C
			s[k] = word[i];
			++k;
		}
	}	
	for(i = 0; i < k; ++i) {
		word[i] = s[i];
	}
	word[i] = '\0';
	return word;
}



char** scan_words(char* buffer, size_t* count) {
	const char* delims = " \t\n,.;";
	size_t size = 0;
	//strtok is pretty insecure, but i'm too bad for strtok_s, plus it's C11 feature	
	for(char *word = strtok(buffer, delims); NULL != word; word = strtok(NULL, delims) ) { 
		++size;
	}

	char** res = (char**)malloc(size * sizeof(char*));

	char *word = buffer;
	for(size_t i = 0; i < size; ++i) {
		size_t len = strlen(word);

		res[i] = (char*)malloc((len + 1) * sizeof(char));

		strcpy(res[i], word);
		clean_word(res[i]);
		word += (len + 1);
	}

	*count = size;
	return res;
}


int compare(const void * p1, const void * p2) {
	const char* const * par1 = (const char* const *)p1;
	const char* const * par2 = (const char* const *)p2; 
	int res = strcmp(*par1, *par2);
	return res;
}

typedef struct WordRecord WordRecord;

struct WordRecord {
	WordRecord * next;
	char * str;
	size_t count;
};

typedef
struct WordList {
	WordRecord *head, *tail;
	size_t size;
} WordList;

WordList* create_WordList() {
	WordList* list = (WordList*)malloc(sizeof(WordList));
	list->head = list->tail = NULL;
	list->size = 0;
	return list;
}

void add_word(WordList* list, const char* word) {
	size_t len = strlen(word);
	char* word_copy = (char*)malloc((len + 1) * sizeof(char));
	if (NULL == word_copy) {
		perror("malloc error");
		exit(13);
	}
	strcpy(word_copy, word);

	WordRecord* node = (WordRecord*)malloc(sizeof(WordRecord));
	if (NULL == node) {
		perror("malloc error");
		exit(13);
	}

	node->str = word_copy;
	node->count = 1;

	if (list->size) {
		list->tail->next = node;
		list->tail = list->tail->next;
	}
	else {
		list->head = list->tail = node;
	}
	
	list->tail->next = NULL;
	++list->size;
}

void free_WordList(WordList* list) {
	WordRecord* next_node;
	for(WordRecord *node = list->head; node != NULL; node = next_node) {
		next_node = node->next;
		free(node->str); node->str = NULL;
		free(node); node = NULL;
	}
	free(list); list = NULL;
}

WordList*
make_unique_and_count(
		     char **words,
		     const size_t count_words
) {
	WordList *list = create_WordList();
	for(size_t i = 0; i < count_words; ++i) {
		if (
		    0 == list->size || 
		    0 != strcmp(list->tail->str, words[i]) 
		) {
			add_word(list, words[i]);
		}
		else {
			++list->tail->count;
		}
	}
	return list;
}




void free_words(char** words, size_t count) {
	for(size_t i = 0; i < count; ++i) {
		free(words[i]); words[i] = NULL;
	}
	free(words); words = NULL;
}

void child_proc() {
	FILE* file = fopen("war_and_peace.txt", "r");
	FILE* sorted_file = fopen("sorted_war_and_peace.txt", "w"); 
	if (NULL == file) {
		perror("failed to open the input file(\"war_and_peace.txt\")");
		exit(6);
	}
	if (NULL == sorted_file) {
		perror("failed to open the file to be written to");
		exit(42);
	}
	char * buffer = file_to_buffer(file, NULL);
	if (NULL == buffer) {
		perror("file_to_buffer error");
		exit(7);
	}
	if (EOF == fclose(file) ) {
		perror("fclose error");
	}
	
	// tokenize buffer to list
	// clean words to remove any undesired symbols
	// count nodes while doing this
	// allocate array and copy list nodes there
	// and finally quicksort
	// then remove duplicates and count
	
	size_t count;
	char ** words = scan_words(buffer, &count);
	free(buffer); 


	qsort(words, count, sizeof(words[0]), compare);
	
	WordList *list = make_unique_and_count(words, count);
	
	free_words(words, count);
	
	
	for(WordRecord *node = list->head; NULL != node; node = node->next) {
		if (0 > fprintf(sorted_file, "%-*s%-zu\n", 40, node->str, node->count) ) {
			perror("fprintf error");
			exit(43);
		}
	}
	
	free_WordList(list);

	if (0 != fclose(sorted_file) ) {
		perror("fclose on sorted_file error");
		exit(44);
	}
	puts("end");
}

int scan_lines(char * buffer, char * * const lines, const size_t count) {
	char * line = strtok(buffer, "\n");
	for(size_t i = 0; NULL != line; ++i) {
		if (i >= count) {
			puts("too many jokes");
			return 1;
		}
		lines[i] = line;
		line = strtok(NULL, "\n");
	}
	return 0;
}

void print_joke(char * jokes[], size_t const joke_count) {
	puts(jokes[(size_t)rand() % joke_count]);
}

void* joker(void * _) {
	FILE * file_jokes = fopen("jokes.txt", "r");
	if (NULL == file_jokes) {
		puts("failed to open \"jokes.txt\"");
		return NULL;
	}
	
	size_t sz;
	char * joke_buffer = file_to_buffer(file_jokes, &sz);
	if (NULL == joke_buffer) {
		return NULL;
	}
	
	if (EOF == fclose(file_jokes) ) {
		perror("fclose error");
		return NULL;
	}
	
	char stack_joke_buffer[sz];
	memcpy(stack_joke_buffer, joke_buffer, sz);

	free(joke_buffer);

#define joke_count (169)
	char * jokes[joke_count];
	
	if (0 != scan_lines(stack_joke_buffer, jokes, joke_count) ) {
		return NULL;
	}

	srand((unsigned)time(NULL) );
	
	
	while(1) {
		print_joke(jokes, joke_count);
		sleep(3);
	}
	
	return NULL;
}

void parent_proc() {
	pthread_t t;
	pthread_attr_t attr;
	if (0 != pthread_attr_init(&attr) ) {
	        perror("pthread_attr_init error");
	        exit(111);
	}
	if (0 != pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) ) {
	        perror("pthread_attr_setdetachstate error");
		exit(545);
	}
	if (0 != pthread_create(&t, &attr, joker, NULL) ) {
		perror("pthread_create error");
		pthread_attr_destroy(&attr); // if it fails then it's the end
		exit(2);
	}
	//if (0 != pthread_detach(t) ) {
	//        perror("pthread_detach error");
	//        exit(3);
	//}
	if (0 != pthread_attr_destroy(&attr) ) {
	        perror("phread_attr_destroy error");
		exit(113);
	}
        wait(NULL);
	exit(0);
}


int main() {
	pid_t pid = fork();
	switch(pid)
	{
                case -1: {
			perror("fork failed");
			exit(1);
		} break;
	        case 0: {
		  child_proc();
		} break;
	        default: {
		  	parent_proc();
		} break;
	}
	return 0;
}
